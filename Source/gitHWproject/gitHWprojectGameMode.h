// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "gitHWprojectGameMode.generated.h"

UCLASS(minimalapi)
class AgitHWprojectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AgitHWprojectGameMode();
};



